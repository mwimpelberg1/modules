variable "server_port" {
  description = "The port that the server will use for HTTP requests"
  type        = number
  default     = 8080
}

variable "cluster_name" {
  description = "The name to use for all of the cluster resources"
  type        = string
}

variable "db_remote_state_bucket" {
  description = "Remote S3 bucket for databases's remote state"
  type        = string
}

variable "db_remote_state_key" {
  description = "The path of the database's remote key"
  type        = string
}


variable "instance_type" {
  description = "The type of instance to run"
  type        = string
}

variable "min_size" {
  description = "ASG minimum size"
  type        = number
}

variable "max_size" {
  description = "ASG max size"
  type        = number
}
