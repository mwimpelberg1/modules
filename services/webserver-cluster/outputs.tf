output "asg_name" {
  value          = aws_autoscaling_group.example.name
  description    = "The name of the autoscaling group"
  }

output "alb_security_group_id" {
  value       = aws_security_group.alb.id
  description = "The ID of the security group attached to the load balancer"
}
